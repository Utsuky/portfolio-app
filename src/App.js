import './App.css';
import 'materialize-css/dist/css/materialize.min.css';
import NavBar from './Ressources/MainPage/NavBar'
import PacMan from './Components/PacMan';

function App() {
  return (
    <div className="App">
      <NavBar/>
      <header className="App-header">
        
      </header>
      <body>
        <PacMan />
      </body>
    </div>
  );
}

export default App;
