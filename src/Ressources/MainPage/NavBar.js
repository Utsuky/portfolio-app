

import React from 'react';

function NavBar() {
    return (
        <nav>
            <div class="nav-wrapper">
                <a href="#" class="brand-logo left fontAudiowide">NF</a>
                <ul id="nav-mobile" class="center hide-on-med-and-down">
                    <li><a class="fontAudiowide" href="sass.html">Home</a></li>
                    <li><a class="fontAudiowide" href="badges.html">About</a></li>
                    <li><a class="fontAudiowide" href="collapsible.html">Resume</a></li>
                    <li><a class="fontAudiowide" href="collapsible.html">Projects</a></li>
                    <li><a class="fontAudiowide" href="collapsible.html">Contact</a></li>
                </ul>
            </div>
        </nav>
    );
}

export default NavBar; 