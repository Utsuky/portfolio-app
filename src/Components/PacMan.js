import React, { useState, useEffect } from 'react';
import './PacMan.css';

const PacMan = () => {
  const [position, setPosition] = useState({ x: window.innerWidth / 2, y: window.innerHeight / 2 });
  const [targetPosition, setTargetPosition] = useState({ x: window.innerWidth / 2, y: window.innerHeight / 2 });
  const [angle, setAngle] = useState(0);
  const [pacManSize, newSize] = useState(0);
  const handleMouseMove = (event) => {
    setTargetPosition({ x: event.clientX, y: event.clientY });
  };

  useEffect(() => {
    window.addEventListener('mousemove', handleMouseMove);
  }, []);

  useEffect(() => {
    const movePacMan = () => {
      const deltaX = targetPosition.x - position.x;
      const deltaY = targetPosition.y - position.y;
      const distance = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
      const speed = 3;

      if (distance > speed) {
        const newX = position.x + (deltaX / distance) * speed;
        const newY = position.y + (deltaY / distance) * speed;
        setPosition({ x: newX, y: newY });

        const newAngle = Math.atan2(deltaY, deltaX) * (180 / Math.PI);
        setAngle(newAngle);
      } else {
        setPosition({ x: targetPosition.x, y: targetPosition.y });
      }
    };


    const intervalIdPacMan = setInterval(movePacMan, 8);
    return () => clearInterval(intervalIdPacMan);
  }, [position, targetPosition]);

  return (
    <div
      className="pacman"
      style={{
        left: position.x - 25,
        top: position.y - 25,
        transform: `rotate(${angle}deg)`,
      }}
    >
      <div className="pacman_eye"/>
      <div className="pacman_mouth" />
    </div>
  );
};

export default PacMan;
